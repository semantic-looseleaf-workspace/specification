# Semantic Looseleaf Workspace Specification

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The 'Semantic Looseleaf Workspace' format and layout is a set of [version-control](https://en.wikipedia.org/wiki/Version_control) and [content-management](https://en.wikipedia.org/wiki/Content_management_system) conventions that lets you generate a portfolio for organizing your creative endeavors using [YAML](https://en.wikipedia.org/wiki/YAML) front matter.  This repository contains the format's specification (currently _quite_ informal.)  Tooling supporting its use lives [here](https://gitlab.com/semantic-looseleaf-workspace/tooling).  

## Prerequisites

 - A [text editor](https://en.wikipedia.org/wiki/List_of_text_editors).  
 - A relatively recent version of [Git](https://en.wikipedia.org/wiki/Git).  
 - Knowledge concerning:  
   - YAML (A quick-start primer may be found located [here](http://web.archive.org/web/20200114195518/https://learnxinyminutes.com/docs/yaml/).)  
 - Some free drive space.  

## Anatomy of a Workspace

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A semantic looseleaf workspace is a Git repository laid out like so[^1]:  

```
$(git rev-parse --show-toplevel) # (Your repository root.)  
├── All Content (Unprocessed)
│   ├── file1.md
│   ├── file2.md
│   ⋮
│   ├── fileN.md
│   └── Shared Metadata.yaml # (Optional)
└── # Any files you might not want to undergo processing.  (Examples:  A read-me and/or a change log, if those don't need any processing.)  
```

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Note that:  

 - The folder 'All Content (Unprocessed)' is your workspace's home for any files you want to get processed for presentation.  
 - Each file stored there needs to start with some prefixed YAML front matter so it can be processed correctly.  The metadata stored in this header is used to sort your content for presentation later.  As a minimum example, each file may have either or both of:  

   - A 'Category' or 'Categories' key.  

     Value:  

     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Either:  

      - A single value or
      - A sequence of one or more child values.  

     Description:  The new or pre-existing top-level folder or folders outside of 'All Content (Unprocessed)' the file should get copied into during processing.  
   - A 'Tag' or 'Tags' key.  

     Value:  

     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Either:  

      - A single value or
      - A sequence of one or more child values.  

     Description:  Keywords with which to index[^3] your content to aid in navigating and searching through it.  

   You can add other arbitrary metadata here, too.  For example, you might want to leave some additional information beyond what the 'Semantic Looseleaf Workspace' format expects here for external tooling to exploit.  It currently won't affect any processing[^2] done to lay your content out for final presentation, though, naturally.  

   Example 1:  

   ```yaml
   ---
   Category:  
    - Stand-Alone Category
   
   # This works, too:  
   Tag:  Stand-Alone Tag
   ---
   ```

   Example 2:  

   ```yaml
   ---
   Categories:  
    - Category 1
    - Category 2
    - Category 3
   
   Tags:  
    - Tag 1
    - Tag 2
    - Tag 3
   ---
   ```

 - All keys, along with their values, from the (optional) 'Shared Metadata' YAML file will get copies of themselves merged into the front matter of every file the Semantic Looseleaf Workspace tooling processes.  'Shared Metadata.yaml' is provided as an internal convenience facility and won't show up outside of 'All Content (Unprocessed.)'  

## Getting Started

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To begin:  

 1. Create a new semantic looseleaf workspace and lay it out as described [above](#Anatomy-of-a-Workspace).  
 1. Run the tooling[^2] used to process a semantic looseleaf workspace.  This will, for each file in 'All Content (Unprocessed:)'  
    - Merge copies of all keys, along with their values, from '`All Content (Unprocessed)/Shared Metadata.yaml`' into its front matter.  
    - Copy it out of 'All Content (Unprocessed)' and into:  
      - Subfolders of your workspace root directory with the same name as each category listed in its front-matter metadata, if any.  
      - Your workspace root directory otherwise.  
    - Insert it wherever it belongs in the tag index.[^3]  
    - Fix any links embedded in it which point to other files in 'All Content (Unprocessed)' up to point to their processed counterparts.[^4]  
 1. Repeat step 2 as needed to make processed files reflect edits made to their unprocessed counterparts in 'All Content (Unprocessed)' and process any new files added there.  

You should now have a semantic looseleaf workspace ready to use (and maybe even share and collaborate on!)  

## Tooling

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Again, tooling for working with repositories following this specification can be found [here](https://gitlab.com/semantic-looseleaf-workspace/tooling).  

## Supported Content File Formats

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;At the moment, the only content file format supported for use inside a semantic looseleaf workspace is:  

 - Markdown

## Current Limitations

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TODO/FIXME:  

 - <a id="Current-Limitations-bullet-point-1"></a>Quoting tooling#1, "The tooling needed to process a semantic looseleaf workspace into a fully presentable, production-ready form has yet to be written."  
 - <a id="Current-Limitations-bullet-point-2"></a>File-format support currently remains quite limited.  See ['Supported Content File Formats'](#Supported-Content-File-Formats) for more details.  (Other formats may work if they support YAML front matter in some way, but their usability is by _no_ means guaranteed to be considered production-ready at this time.)  (Note also tooling#5.)  
 - <a id="Current-Limitations-bullet-point-3"></a>Tag indexing has yet to be implemented.  No tag index page will get generated.  (Fixing this is tracked by tooling#2.)  
 - <a id="Current-Limitations-bullet-point-4"></a>Similarly, link fix-up has yet to be implemented.  (Fixing this is tracked by tooling#3.)  

## Inspirations/Background<!--Section title heading possibly subject to change/revision…?  -->

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The 'Semantic Looseleaf Workspace' format takes inspirational cues from:  

 - [GitHub user refractorsaurusrex](https://github.com/refactorsaurusrex)'s ['`journal-cli`' project](https://github.com/refactorsaurusrex/journal-cli).  
 - [Static web site generators](https://en.wikipedia.org/wiki/Web_template_system#Static_site_generators) like [Jekyll](https://en.wikipedia.org/wiki/Jekyll_(software)).  

Any further idiosyncrasies in this project derive from its author&thinsp;—&thinsp;that is, me.  

---

## Endnotes

[^1]:  See [the 'Current Limitations' section](#Current-Limitations), [bullet point 2](#Current-Limitations-bullet-point-2).  
[^2]:  See [the 'Current Limitations' section](#Current-Limitations), [bullet point 1](#Current-Limitations-bullet-point-1).  
[^3]:  See [the 'Current Limitations' section](#Current-Limitations), [bullet point 3](#Current-Limitations-bullet-point-3).  
[^4]:  See [the 'Current Limitations' section](#Current-Limitations), [bullet point 4](#Current-Limitations-bullet-point-4).  