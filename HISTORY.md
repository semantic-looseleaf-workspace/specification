# Repository History (Change Log)

For full details, see the Git logs.  

## v0.0.1, pre-alpha 0.0.2.1, post-release fix-up 1 (v0.0.1.pre-alpha_0.0.2.1.post_1) (Friday, April 16<sup>th</sup>, 2021)

 - Versioning:  
   - (_Minor Addition_) Make affordances for small adjustments before and after proper releases:  
     - Allow releases' machine-readable version segments to contain 'pre' or 'post,' followed by first an underscore, then a number.  
     - Make a similar tweak for prose versions.  
 - README.md:  
   - Current Limitations:  Fix the syntax of issue references in this section accidentally broken when the previous release updated them to reflect that their link targets had changed repositories.  

## v0.0.1, pre-alpha 0.0.2.1 (v0.0.1.pre-alpha_0.0.2.1) (Wednesday, April 14<sup>th</sup>, 2021)

 - Versioning:  
   - (_Minor Tweak_) Introduce a new prose version format identical to the machine-readable one except in that it uses:  

     - ',&nbsp;' (a comma and immediately-subsequent space) instead of a period ('.') to separate a version's 'major,' 'minor,' and 'patch' segments from its pre-release type designator.  
     - A single space ('&nbsp;') instead of an underscore ('_') to separate its pre-release type designator from its pre-release version segment.  

     (This format was already in use for Git tags' descriptions.  The tags themselves, of course, are still named using the machine-readable version format.)  
 - README.md:  
   - Apply a few marginal syntax and prose style fixes.  
   - Update it to reflect that Semantic Looseleaf Workspace tooling has now gotten split out into its own, distinct repository.  
   - Reference this external tooling explicitly in a couple sections.  
 - HISTORY.md:  
   - Prefix all top-level changelog entries' headings with the version in its new prose format.  

## v0.0.1, pre-alpha 0.0.2.0 (v0.0.1.pre-alpha_0.0.2.0) (Monday, March 15<sup>th</sup>, 2021)

 - Versioning:  
   - [_**Breaking Change!**_] Switch to using a new version-numbering scheme:  

     - Rename the previous release, 'v0.0.1_pre-alpha_1,' to 'v0.0.1.pre-alpha_0.0.1.0.'  
     - Now, retroactively:  
       - Designation of a version's pre-release status&thinsp;—&thinsp;'pre-alpha,' 'alpha,' 'beta,' 'RC (**R**elease **C**andidate,)' etc.&thinsp;—&thinsp;now uses its own segment of a release's version number, distinctly separate from its top-level 'patch' segment.  
       - Release version numbers' pre-release type designators still share a version number segment with the 'major' segment of the pre-release release version type they denote.  
       - Release version numbers' full pre-release segments now contain distinctly separate 'pre-release major,' 'pre-release minor,' 'pre-release patch,' and 'pre-release sub-patch' sub-segments.  (This improves the granularity of pre-release version reporting.)  

     In summary, the new version number format is:  '_&lt;major&gt;_`.`_&lt;minor&gt;_`.`_&lt;patch&gt;_`.`_&lt;pre-release type designator&gt;_`_`_&lt;pre-release major&gt;_`.`_&lt;pre-release minor&gt;_`.`_&lt;pre-release patch&gt;_`.`_&lt;pre-release sub-patch&gt;_.'  
 - README.md:  
   - Apply one small grammar fix in its 'Getting Started' section.  

## v0.0.1, pre-alpha 0.0.1.0 (v0.0.1.pre-alpha_0.0.1.0) (Tuesday, August 4<sup>th</sup>, 2020)

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(For historical purposes, this was previously release 'v0.0.1_pre-alpha_1.')  

 - Initial public preview release.  (🎉!)  